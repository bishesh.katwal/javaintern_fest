/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.payrollSystem.entity.common;

import com.payrollSystem.entity.abstracts.AbstractCode;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author Bishesh Katwal
 */
@Getter
@Setter
@Entity
@Table(name = "FEE_GROUP")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FeeGroup.findAll", query = "SELECT f FROM FeeGroup f"),
    @NamedQuery(name = "FeeGroup.findById", query = "SELECT f FROM FeeGroup f WHERE f.id = :id")
 })
    
public class FeeGroup extends AbstractCode {

    @Column(name ="ISACTIVE", nullable = false)
    private boolean isActive=true;
    
}
