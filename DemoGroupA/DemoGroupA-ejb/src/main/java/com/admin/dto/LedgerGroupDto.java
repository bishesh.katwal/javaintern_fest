/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.admin.dto;

import com.admin.constant.LedgerGroupMainConstant;
import com.admin.dto.abstracts.AbstractCodeDto;
import lombok.Getter;
import lombok.Setter;


/**
 *
 * @author Bishesh Katwal
 */
@Getter
@Setter
public class LedgerGroupDto extends AbstractCodeDto{
    
    private String mainGroup;
    
    public LedgerGroupMainConstant[] getLedgerGroupMainConstants(){
        return LedgerGroupMainConstant.values();
    }
}
